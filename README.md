# IA5BS 

A Deep Neural Network to simultaneously predict secondary structure and
transmembrane parts.

## Abstract

In this project, we developped a deep learning model capable of predicting
both the secondary structure and the transmembrane parts of a protein from 
its amino acids sequence. This model is able to learn from two disjoints 
datasets: there is no requirement for a dataset containing sequences 
and both informations for each sequence. We showed that eventhough 
the model predictions are limited, it is still able to learn from two 
disjoints datasets and thus that with more work this kind of multi-task learning 
may be able to provide high quality predictions. Moreover, we created a reusable 
Python class architecture that allows for easily switching the neural networks
architecture for this kind of task.

## Requirements and use

### Requirements

- Python>=3.6
- matplotlib
- numpy
- pandas
- scikit_learn
- seaborn
- tensorflow>=2.7.0
- tensorflow_gpu>=2.7.0

### Use

The script `src/train_model.py` trains the model for a given number of epochs.
The losses and metrics to track are defined within the script.
Its options are:
- `-d` path to the training directory. The script expects to find 2 csv files
  formatted like the files in the `data/train` directory of this repository.
- `-o` path to store the model and its results. The script will create a
  subdirectory for the model.
- `-t` Number of epochs to run the training for. 
- `-b1` Size of batch for the secondary structures. Expressed not in number of
  sequences but in number of subsequences of length `window`. Defaults to 100.
- `-b1` Size of batch for the transmembrane parts. Expressed not in number of
  sequences but in number of subsequences of length `window`. Defaults to 100.
- `-c` number of sequences from the secondary structure dataset to keep.
  Used to make training possible on machines with limited computing power, as
  the secondary structure dataset may contain too much sequences to handle.
- `-e` activate early stopping in training

The script `src/evaluate_model.py` plots the loss values and metrics values 
during training and computes the confusion matrix on the testing dataset, 
to evaluate the performances of a trained model. Its options are:
- `-m` directory to the saved model 
- `-t` directory to the testing dataset. Should be formatted like the 
  `data/test` directory of this repository
- `-c` number of sequences from the secondary structure dataset to keep.

The script `src/predict_from_fasta` takes a fasta file as an input and 
predicts the secondary structures and transmembrane parts of the sequences 
included in the fasta file. Its arguments are:
- `-m` directory to a saved model to use for prediction
- `-f` path to the fasta file containing the sequences to run the predictions
for.
This scripts creates two files where the fasta file is: a .secstr file
containing the predicted secondary structures, and a .tm file containing
the predicted transmembrane parts, both formatted in a fasta-like way.
  
The script `run.sh` creates a directory to save models to, train a model
and evaluate it. It has no option, changing training parameters should be 
done by editing this script if you want to use this script.

## Data

We could find the original datas in the 'Dataset' file.

These dataset were randomly split in 3: a train, validation and test part
with a 80%/10%/10% split.

### Secondary structure
For secondary structures, we combined : 

-  The data used by Bepler and Berger ([5], [tbepler/protein-sequence-embedding-iclr2019](https://github.com/tbepler/protein-sequence-embedding-iclr2019)) This dataset contains 20784 proteins from the PDB. It is presented in 3 documents in .fasta format: Training set, Test set and Total set.

- The dataset previously used in the lab, which contains 477.210 proteins from the PDB.

For these two datasets the secondary structure is coded with DSSP algorithm.

### Transmembrane parts
For transmembrane parts, we combined : 

- The datasets used by [TMHMM](https://services.healthtech.dtu.dk/services/TMHMM-2.0/TMHMM/index.html) [3], he contains 160 transmembrane proteins and 645 proteins located entirely within the cell, from the PDB. The transmembrane position information is given by a sequence of 'i' (input) 'o' (output) and 'M' (membrane).

- The datasets used by Bepler and Berger, TOPCONS2 ([4], [tbepler/protein-sequence-embedding-iclr2019](https://github.com/tbepler/protein-sequence-embedding-iclr2019)), he contains 286 transmembrane proteins and 2927 proteins located entirely within the cell, from UniProt. The transmembrane position information is given by a sequence of 'I' (input) 'O' (output) and 'M' (membrane). We also could find sequences with their peptide signal (name file with '+SP').


## Model \& Methods

We chose to train an architecture based on the one used by 
[computbiolgeek/massp](https://github.com/computbiolgeek/massp) [1]. The 
chosen architecture is thus a Convolutionnal Neural Network. We found while
experimenting with different architectures that CNNs performed as well as
Long Short-Term Memory (LSTM) networks yet using less weights and thus being 
faster to train.
Our model predicts, for each amino acid:
- to which class of secondary structure it belongs, among 3 classes (Q3):
helix, sheet or no particular structure.
- if the given amino acid is in solution or in the membrane.
We split sequences with a window of 21, each time predicting the classes 
the amino acid at the center of the window belongs to.

We chose to implement our model using Tensorflow and its Keras API.
Our architecture has two main differences from the one used in MASSP:
- First, we were not able to get Position-Specific Scoring Matrix for the
sequences of our datasets. Indeed, computing them would have taken too 
much time to complete this project. Thus, we replaced PSSMs by a Keras 
`Embedding` layer, which is able to learn an embedding, meaning it learns a
way to convert an amino acid to a vector, which means a protein is converted
to matrix. We hoped that the embedding layer would be able to roughly 
emulate the effect of PSSMs.
- Then, as we did not have a joint dataset with both secondary structure and 
transmembrane parts for each sequence, we actually trained 2 models, one
that predicts the secondary structure and another that predicts the 
transmembrane parts. These two models share layers that are defined outside of
the Keras Sequential Models. As these classes do not do deep copies of the 
layers when they are created, it means that when the gradient is applied to 
the layer of both models. As the tasks of predicting secondary structure and 
transmembrane parts should use common informations from the sequence,
we hope a multi-task approach would increase performances compared to single 
task predictions.

The CNN architecture is the following:<br>

![Architecture diagram](images/ugly_diagram.jpg){ height=50% margin=auto }

Common layers with synchronized weights are figured with an arrow between them.

We then train both models alternatively, meaning that at each epoch, we train 
the secondary structure model then the transmembrane model.

For the secondary structure model, we chose to follow accuracy during training,
which is the number of correct predictions divided by the total number of 
predictions.

For the transmembrane model, we followed both Recall and Precision:
- Recall = TP/(TP+FN)
- Precision = TP/(TP+FP)
With:
- TP: True Positives
- TN: True Negatives
- FP: False Positives
- FN: False Negatives

As the number of amino acids in solution far outnumbers the number of
transmembrane amino acids, the recall is especially relevant, as it is
the proportion of correctly predicted transmembrane amino acids.

Furthermore, as training time was limited, we implemented the models in classes
that separates the training from the architecture, allowing from quickly 
modifying the architecture.

## Results

We trained the model for 500 epochs, with the full transmembrane dataset but
with a reduced number of secondary structures (20,000) as our computing was 
limited.

![losses](images/Losses.png)<br>
![Accuracy](images/secstr_Accuracy.png)<br>
![Recall](images/tm_Recall.png)<br>
![Precision](images/tm_Precision.png)<br>

As we can see, the model managed to learn from the dataset, but still 
shows limited results, with a Q3 accuracy of 67% and a recall of 65%, meaning
that 65% of transmembrane residues correctly classified

We also compute the confusion matrix on the test dataset (normalized by row for
an easier reading) that the secondary structure mainly struggle to classify
sheets, while the transmembrane missclassify an important number of 
transmembrane residues.

![matrix1](images/confusion_secstr.png) <br>
![matrix2](images/confusion_tm.png) <br>


## Discussion \& possible ameliorations

As shown by the results, eventhough our model has learned from the data, it 
shows limited predictive capabilities. However, it appears from the evolution 
of the loss function during training that the model is not overfitting, which 
means weights could be added to the architecture to obtain a better fit.

It is to be noted that the use of 2 separated datasets and thus 2 separated 
models may impact the performances, as during training, the two differents 
may pull the common layers weights in different directions.
More training time with larger data and adjusting of the optimizers and
learning rates may increase the performances of our model.

Furthermore as the architecture we used is really simple, it could be
interesting to make it more complex. Others things that could be interesting
to add to the architecture may be max pooling between the convolutionnal layers
instead of after, or adding a more fully connected layers in the prediction-specific
parts of the model.

A first way to improve the model itself would be to use PSSMs as input to the model 
instead of using the amino acids sequence. Indeed, most of the models reporting 
high accuracies seem to use PSSMs as input.

A specifically trained embedder could also be used instead of the Keras `Embedding`
layer, such as the embedder developped by Bepler and Berger [2] 
([tbepler/prose](https://github.com/tbepler/prose)). We considered using this 
embedder, yet it produces embedding with too much dimensions to be used on a 
machine with limited memory. An even better solution would be to apply transfer
learning and retrain such a pretrained embedder, yet the prose embedder is 
written in PyTorch and we failed to convert it to ONNX, the open source 
neural network format.

## References

[1]: Li B, Mendenhall J, Capra JA, and Meiler J, A multi-task deep-learning method
for predicting membrane associations and secondary structures of proteins, J.
Proteome Res. 2021, 20, 8, 4089–4100

[2]: Bepler, T., Berger, B. Learning the protein language: evolution,
structure, and function. Cell Systems 12, 6 (2021)

[3]: S. Moller, M.D.R. Croning, R. Apweiler. Evaluation of methods for the
prediction of membrane spanning regions. Bioinformatics, 17(7):646-653, July
2001

[4]: Tsirigos, K.D., Peters, C., Shu, N., Kall, L., Elofsson, A., 2015. The
TOPCONS web server for consensus prediction of membrane protein topology and
signal peptides. Nucleic Acids Res. 43, W401-W407

[5]: Bepler, T., Berger, B. Learning protein sequence embeddings using information
from structure, International Conference on Learning Representations, 2019
