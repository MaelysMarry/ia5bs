import os
import subprocess
from argparse import ArgumentParser
import re
import csv
import pandas as pd
import pdb_to_fasta


def download_uniref(uniref, data_dir):
    """ Downloads the uniref file with wget
        Extract if necessary

    Args:
        uniref (`str`): url of the uniref file to download
        data_dir (`str`): path of the directory for writing file

    Returns:
        file (`str`): path of the downloaded file

    """
    # Checking data directory
    check_dir(data_dir)

    data_dir = os.path.join(data_dir, "uniref")
    check_dir(data_dir)

    # Downloading file
    file = uniref.split('/')[-1]  # Name of the future file
    file = os.path.join(data_dir, file)
    if os.path.isfile(file):
        os.remove(file)
    subprocess.run(["wget",
                    uniref,
                    "-P",
                    data_dir])
    if ".gz" in file:  # Extracting if compressed
        print(f"Extracting {file}")
        subprocess.run(["gunzip", file])
        file = file.replace(".gz", "")
    files.append(file)

    return file


def check_downloaded_file(uniref, data_dir):
    """ Check if the uniref file is present. Extract if necessary 

    Args:
        file (`str`): url of the uniref file
        data_dire (`str`): path of the directory for writing file

    Returns:
        file (`str`): path to the uniref file

    """

    file = uniref.split('/')[-1]
    file = file.replace(".gz", "")
    file = os.path.join(data_dir, "uniref", file)
    if not os.path.exists(file):
        if os.path.exists(f"{file}.gz"):
            # if compressed version exists
            print(f"Extracting {file}...")
            subprocess.run(["gunzip", file])
        else:
            raise Exception(f"Missing file {file.split('/')[-1]}")
    return file


def run_psiblast(prot_id, sequence, uniref, directory):
    """ Run psiblast for a sequence, write the sequence as a fasta file 
    and write the pssm matrix
    """

    pssm_path = os.path.join(directory, 'pssm', f"{prot_id}.pssm")
    fasta_path = os.path.join(directory, 'fasta', f"{prot_id}.fa")

    with open(fasta_path, 'w') as f:
        f.write(f">{prot_id}\n")
        f.write(sequence)

    subprocess.run(['psiblast',
                    '-query', fasta_path,
                    '-db', uniref,
                    '-save_pssm_after_last_round',
                    '-out_ascii_pssm', pssm_path])


def process_df(df, directory, uniref):
    """Run psiblast for each sequence in the dataframes
    """

    check_dir(directory)
    train = df.sample(frac=.8)
    test = df.drop(train.index)

    train_dir = os.path.join(directory, 'train')
    check_dir(train_dir)

    test_dir = os.path.join(directory, 'test')
    check_dir(test_dir)

    check_dir(os.path.join(train_dir, 'pssm'))
    check_dir(os.path.join(train_dir, 'fasta'))
    check_dir(os.path.join(train_dir, 'labels'))

    check_dir(os.path.join(test_dir, 'pssm'))
    check_dir(os.path.join(test_dir, 'fasta'))
    check_dir(os.path.join(test_dir, 'labels'))

    train.apply(lambda x: run_psiblast(x['id'], 
                                       x['Sequences'], 
                                       uniref, train_dir), 
                                       axis=1)

    test.apply(lambda x: run_psiblast(x['id'], 
                                      x['Sequences'], 
                                      uniref, test_dir),
                                      axis=1)


def check_dir(directory):
    """Check if directory exists, is a directory and creates it if necessary
    """

    if not os.path.exists(directory):
        os.mkdir(directory)
    elif not os.path.isdir(directory):
        os.mkdir(directory)


def main():

    parser = ArgumentParser(
        description="This scripts downloads and processes" +
                    "the data necessary for the training" +
                    "of the prediction model"
    )

    parser.add_argument("-o", "--out",
                        help="directory to write the data to",
                        required=True)
    parser.add_argument("-sdl", "--skip_download",
                        help="skip download of the files",
                        action="store_true")
    parser.add_argument("-sdb", "--skip_database",
                        help="skip making of the database",
                        action="store_true")
    parser.add_argument("-sp", "--skip_pssm",
                        help="compute pssm",
                        action="store_true")
    parser.add_argument("-t", "--threads",
                        help="number of threads to use to compute the PSSMs",
                        type=int,
                        default=1)

    args = parser.parse_args()
    data_dir = args.out
    skip_dl = args.skip_download
    skip_db = args.skip_database
    skip_ps = args.skip_pssm
    threads = args.threads

    uniref = "ftp://ftp.uniprot.org/pub/databases/uniprot/uniref/uniref50/uniref50.fasta.gz"

    if not skip_dl:
        print('Downloading Uniref')
        uniref = download_uniref(uniref, data_dir)
    else:
        # Checking that the the uniref file is present if download has been skipped
        print('Checking if files are present')
        uniref = check_downloaded_file(uniref, data_dir)

    if not skip_db:
        print('Making blast database')
        subprocess.run(['makeblastdb',
                        '-in', uniref,
                        '-dbtype', 'prot'])
    else:
        # Check if db is present
        pass

    df_secstr = pd.read_csv(os.path.join(data_dir, 'secstr_Real20784.csv'))
    df_tm = pd.read_csv(os.path.join(data_dir, 'TMHMM_Real805.csv'))

    df_secstr = df_secstr.rename(columns={'Unnamed: 0': 'id',
                                          'str2aire': 'label'})
    df_tm = df_tm.rename(columns={'Unnamed: 0': 'id',
                                  'Membrane': 'label'})

    print(df_secstr.head())

    process_df(df_secstr, os.path.join(data_dir, 'secstr'), uniref)
    process_df(df_tm, os.path.join(data_dir, 'trasmembrane'), uniref)


if __name__ == "__main__":
    main()
