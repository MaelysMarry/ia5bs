from models.model import ProtModel
from tensorflow.keras import layers


class CNNmodel(ProtModel):
    """This class defines the architecture of a ProtModel.
    This architecture comes from https://github.com/computbiolgeek/massp
    """

    def __init__(self, window, embedding_size=20, alphabet_size=21):
        common_layers = [
            layers.Input(shape=[window]),
            layers.Embedding(alphabet_size, embedding_size),
            layers.Reshape((window, embedding_size, 1)),
            layers.Conv2D(16, (3, 3), activation='relu'),
            layers.Conv2D(32, (3, 3), activation='relu'),
            layers.MaxPooling2D((2, 2)),
            layers.Flatten(),
            layers.Dense(64, activation='relu'),
            layers.Dropout(.2)
        ]
        secstr_layers = [
            layers.Dense(64, activation='relu'),
            layers.Dropout(.2),
            # p-ê rajouter couche dense 32
            layers.Dense(3, activation='softmax'),
        ]
        tm_layers = [
            layers.Dense(32, activation='relu'),
            layers.Dropout(.2),
            layers.Dense(1, activation='sigmoid'),
        ]
        super().__init__(common_layers, [secstr_layers, tm_layers], window)
