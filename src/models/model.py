import os
import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
from models.data_utils import vocab_prot, vocab_secstr, vocab_tm
from models.data_utils import encode_x, encode_y, transform_df, check_dir
import sklearn.metrics


gpus = tf.config.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        print(e)


class MTModel:
    """Boilerplate class that wrap around several keras Models and trains them
    alternatively. This class is intended for models with a common part and
    same shape of inputs.

    Attributes:
        models: list of keras models, these models should have a common part

    """

    def __init__(self, models):
        """__init__ of the MTmodel class, creates an instance from an iterable
        containing keras models

        Args:
            models: a list of keras models.

        """

        self.models = []
        for model in models:
            self.models.append(model)

    def __iter__(self):
        """Made the class iterable. Iterating through it is equivalent to
        iterating though the different models.

        Returns:
            an iterator made from the models list

        """

        return iter(self.models)

    @classmethod
    def from_layers(cls, common_layers, specific_layers):
        """Creates the class from lists of keras layers: a part common to
        all models and a list of keras layers for each model.

        Args:
            common_layer: a list of keras layers
            specific_layers: a list of list of keras layer. A model will
                be created for each list of keras layers in this argument

        Returns:
            An instance from this class

        """

        models = []
        for model_layers in specific_layers:
            models.append(keras.Sequential(common_layers + model_layers))
        return cls(models)

    @classmethod
    def load(cls, directory):
        """Create the class from the given directory. A keras model will be
        created for each subdirectory in the directory. Models should be
        saved in the tensorflow Saved Model format.

        Args:
            directory (`str`): the path to the directory to create the models
                from.

        Returns:
            An instance from this class

        """
        models = []
        for i, model in enumerate(os.listdir(directory)):
            if model.split("_") == "model":
                models.append(keras.models.load_model(os.path.join(directory,
                                                                   model)))
        return cls(models)

    def compile(self, losses, opts, metrics):
        """Add a loss, optimizer and metrics to each keras model with the
        keras compile method.

        Args:
            losses: list of loss functions from keras.metrics or defined
                accordingly. One loss for each model defined.
            opts: list of optimizers function from keras or defined
                accordingly. One optimizer for each model defined.
            metrics: list of list of metrics function from keras or defined
                accordingly. One list of metrics for each model

        """

        assert len(losses) == len(self.models)
        assert len(opts) == len(self.models)
        assert len(metrics) == len(self.models)

        for model, loss, opt, metric in zip(self.models,
                                            losses,
                                            opts,
                                            metrics):
            model.compile(optimizer=opt, loss=loss, metrics=metric)

    def fit(self, ds_train, ds_val,
            epochs, batch_sizes, early_stopping=False):
        """Fit the models alternatively: in each epoch, each model is trained

        Notes:
            All list of datasets should have a length equal to the number of
            models.
            All x[i] should have the same first dimension as y[j]

        Args:
            ds_train: tensorflow dataset for training
            ds_val: tensorflow dataset for validation
            epochs: number of epochs to run the training for
            batch_sizes: list of batch sizes, one batch size for each model
            early_stopping: boolean, determines if early stopping is applied

        Returns:
            train_loss: value of the loss funtion during training for each
                epoch and each epoch. numpy array where the first dimension is
                the number of epochs and the second dimension the number of
                models
            val_loss: value of the loss function during validation. Formated
                like train_loss
            metrics: list (one element for each model) of dictionaries of
                metrics each entry in the dictionary is the time evolution of a
                metric along the training

        """

        assert len(batch_sizes) == len(self.models)
        assert len(ds_train) == len(self.models)
        assert len(ds_val) == len(self.models)

        train_loss = np.zeros((epochs, len(self.models)))
        val_loss = np.zeros((epochs, len(self.models)))
        metrics = [{} for i in self.models]

        train_flag = [True for m in self.models]  # Flags for early stopping

        for i in range(len(self.models)):
            ds_train[i] = ds_train[i].batch(batch_sizes[i])
            ds_val[i] = ds_val[i].batch(batch_sizes[i])

        for i in range(epochs):
            print(f"epoch {i+1}/{epochs}")
            for j, model in enumerate(self.models):
                if train_flag[j]:
                    print(f"fitting model {j+1}/{len(self.models)}")
                    history = model.fit(ds_train[j], epochs=1)
                    train_loss[i, j] = history.history['loss'][0]

            for j, model in enumerate(self.models):
                print(f"evaluating model {j+1}/{len(self.models)}")
                results = model.evaluate(ds_val[j])
                val_loss[i, j] = results[0]
                for result, name in zip(results[1:], model.metrics_names[1:]):
                    if name in metrics[j]:
                        metrics[j][name].append(result)
                    else:
                        metrics[j][name] = []
            print('\n')

            # Checking if val loss has increased
            if early_stopping:
                for j, flag in enumerate(train_flag):
                    mean_30_last = np.mean(train_loss[-30:, j])
                    mean_30_before = np.mean(train_loss[-60:-30, j])
                    if mean_30_last > mean_30_before and flag:
                        train_flag[j] = False

            if early_stopping and not any(train_flag):
                break

        return train_loss, val_loss, metrics

    def predict(self, inputs):
        """Computes a prediction from each model from the given inputs.
        The same input is fed to each model. You should ensure that the
        inputs is compatible with ever model.

        Args:
            inputs: inputs given to each model

        """

        preds = []

        for model in self.models:
            preds.append(model.predict(inputs))

        return preds

    def save(self, directory):
        """Save every keras model in the Saved Model tensorflow format

        Args:
            directory (`list`): path to the directory to save the model to.
                creates a subdirectory for each model.

        """

        check_dir(directory)
        for i, model in enumerate(self.models):
            model.save(os.path.join(directory, f"model_{i}"))


class ProtModel(MTModel):
    """Child class of the previous one: it is specially adapted to predict
    secondary structure and transmembrane informations from pandas dataframe.
    It is able to filter and transform pandas dataframes into data that keras
    architectures can manage.
    It can be either used directly or with a child class that defines its
    architecture in its __init__ and calls super().__init__

    Attributes:
        window ('int'): size of the window used to split each sequence
            into subsequences of length 'window'. A rolling window is
            applied. Should be odd.
        vocab_x: dictionary that map each amino acid to an integer
        vocabs_y: tuple of 2 dictionaries that map the symbols in each
            dataset's ground truth to integers.
        xs_train: dataset of sequences used for training
        ys_train: dataset of labels used for training
        xs_val: dataset of sequences used for validation
        ys_val: dataset of labels used for validation

    """

    def __init__(self, common_layers, specific_layers, window):
        """Create an instance of this class from common layers and specific
        layers for each model

        Args:
            common_layers: list of keras layers common to both models
            specific_layers: list of two lists of keras layers,
                1st list with the layers for the part specific to
                secondary structure prediction and 2nd list containing
                the parts specific to the transmembrane models.

        """

        models = [keras.Sequential(common_layers + model_layers)
                  for model_layers in specific_layers]
        super().__init__(models)
        self.window = window
        self.vocab_x = vocab_prot()
        self.vocabs_y = (vocab_secstr(), vocab_tm())
        self.inv_vocabs = [{v: k for k, v in voc.items()}
                           for voc in self.vocabs_y]

    @classmethod
    def load(cls, directory):
        """Create the class from the given directory. A keras model will be
        created for each subdirectory in the directory. Models should be saved
        in the tensorflow Saved Model format.
        This method is a version of load that searchs for common layers between
        models. Using models with no common layers may cause an undefined
        behavior.

        Args:
            directory (`str`): the path to the directory to create the models
                from.

        Returns:
            An instance from this class

        """
        models = []
        for i, model in enumerate(os.listdir(directory)):
            if model.split("_")[0] == "model":
                models.append(keras.models.load_model(os.path.join(directory,
                                                                   model)))

        # Searching for common layers
        layers = [m.layers for m in models]
        max_len = max([len(layer) for layer in layers])
        common_layers = []
        max_layer = 0  # last layer added to common_layers
        for i in range(max_len):
            flag = True
            name = layers[0][i].name
            for layer in layers:
                if layer[i].name != name:
                    flag = False
                    break
            if flag:  # if all layers at position i have the same name
                common_layers.append(layer[i])
                max_layer = i
        # The rest of the layers:
        specific_layers = [layer[max_layer + 1:] for layer in layers]

        # getting the window of the model
        window = [line for line in open(
            os.path.join(directory, 'window.txt'), 'r')]
        window = window[0].replace('\n', '')
        window = int(window)

        return cls(common_layers, specific_layers, window)

    def save(self, directory):
        """Save every keras model in the Saved Model tensorflow format by
        calling the parent save method.
        Aditionally, saves the window of the model in a window.txt file

        Args:
            directory (`list`): path to the directory to save the model to.
                creates a subdirectory for each model.

        """

        super().save(directory)
        with open(os.path.join(directory, 'window.txt'), 'w') as f:
            f.write(f"{self.window}\n")

    def load_data(self, df_secstr, df_tm, split=.8, cut_off=100):
        """Load and transforms the datasets for training into the
        model

        Notes:
            each pandas dataframe should contain a Sequences column containing
            the amino acids sequences and a Targets column containing
            respectively the secondary structure and the transmembrane data

        Args:
            df_secstr: pandas dataframe containing sequences and secondary
                structure informations formatted as said above.
            df_tm: pandas dataframe containing sequences and transmembrane
                informations
            split: fraction of the dataset used for training. 1-split %
                of the dataset is then used for validation (here called
                val because it is shorter).
            cut_off: minimum size of sequences to keep

        """

        dfs = (df_secstr, df_tm)
        dfs = (df[~df.Sequences.str.contains(r'[^ARNDCQEGHILKMFPSTWYVUO]')]
               for df in dfs)
        dfs = [df[df['Sequences'].str.len() >= cut_off] for df in dfs]
        train = [df.sample(frac=split) for df in dfs]
        val = [df.drop(tr.index) for df, tr in zip(dfs, train)]

        xy_train = [transform_df(tr,
                                 encode_x, encode_y,
                                 self.vocab_x, vocab_y,
                                 self.window)
                    for tr, vocab_y in zip(train, self.vocabs_y)]
        xy_val = [transform_df(te,
                               encode_x, encode_y,
                               self.vocab_x, vocab_y,
                               self.window)
                  for te, vocab_y in zip(val, self.vocabs_y)]

        xs_train = [xy[0] for xy in xy_train]
        ys_train = [xy[1] for xy in xy_train]

        xs_val = [xy[0] for xy in xy_val]
        ys_val = [xy[1] for xy in xy_val]

        # Converting numpy array to tensorflow tensors
        # Not sure if necessary
        for i, x in enumerate(xs_train):
            xs_train[i] = tf.convert_to_tensor(x)
        for i, x in enumerate(xs_val):
            xs_val[i] = tf.convert_to_tensor(x)
        for i, y in enumerate(ys_train):
            ys_train[i] = tf.convert_to_tensor(y)
        for i, y in enumerate(ys_val):
            ys_val[i] = tf.convert_to_tensor(y)

        self.ds_train = []
        self.ds_val = []
        AUTOTUNE = tf.data.AUTOTUNE
        # Converting numpy arrays to tensorflow datasets
        for i in range(len(self.models)):
            self.ds_train.append(
                tf.data.Dataset.from_tensor_slices((xs_train[i], ys_train[i]))
            )
            #self.ds_train[i] = self.ds_train[i].batch(batch_sizes[i])
            self.ds_train[i] = self.ds_train[i].cache().prefetch(buffer_size=AUTOTUNE)
        for i in range(len(self.models)):
            self.ds_val.append(
                tf.data.Dataset.from_tensor_slices((xs_val[i], ys_val[i]))
            )
            #self.ds_val[i] = ds_val[i].batch(batch_sizes[i])
            self.ds_val[i] = self.ds_val[i].cache().prefetch(buffer_size=AUTOTUNE)

        print(
            f"Secondary structure training set size: "
            f"{xs_train[0].shape[0]}"
        )
        print(f"Transmembrane training set size: {xs_train[1].shape[0]}")
        print('\n')

    def fit_from_data(self, epochs, batch_sizes=(100, 100),
                      early_stopping=False):
        """Fit the model from the data previously loaded

        Notes:
            cannot be used before load_data

        Args:
            epochs: number of epochs to run the training for
            batch_sizes: list of batch sizes, 1st one for the secondary
                structure model, 2nd one the transmembrane model
            early_stopping: boolean, determines if early stopping is applied

        Returns:
            secstr: train_loss, test_loss and metrics for secondary structure
                model, along time
            tm: id for transmembrane model

        """

        train_loss, val_loss, metrics = self.fit(self.ds_train,
                                                 self.ds_val,
                                                 epochs=epochs,
                                                 batch_sizes=batch_sizes,
                                                 early_stopping=early_stopping)

        secstr = {'train_loss': train_loss[:, 0],
                  'val_loss': val_loss[:, 0],
                  'metrics': metrics[0]}
        tm = {'train_loss': train_loss[:, 1],
              'val_loss': val_loss[:, 1],
              'metrics': metrics[1]}

        return secstr, tm

    def confusion_matrix(self, normalize=None):
        """Computes the confusion matrix for model prediction, from the data
        loaded with load data. Intended to be used with the testing dataset/a
        dataset the model was not trained with.

        Returns:
            a dictionary containing the two confusion matrices.

        """

        ds_secstr = tf.data.Dataset.concatenate(
            self.ds_train[0],
            self.ds_val[0]
        )
        ds_tm = tf.data.Dataset.concatenate(
            self.ds_train[1],
            self.ds_val[1]
        )

        x_secstr = np.array([x for x, y in ds_secstr])
        y_pred_secstr = self.models[0].predict(x_secstr)
        y_pred_secstr = tf.argmax(y_pred_secstr, axis=1)
        y_true_secstr = tf.concat([y for x, y in ds_secstr], axis=0)

        matrix_secstr = sklearn.metrics.confusion_matrix(
            y_true_secstr,
            y_pred_secstr,
            normalize=normalize
        )

        x_tm = np.array([x for x, y in ds_tm])
        y_pred_tm = self.models[1].predict(x_tm)
        #y_pred_tm = tf.argmax(y_pred_tm, axis=1) # je suis con
        y_pred_tm = y_pred_tm >= .5
        y_true_tm = tf.concat([y for x, y in ds_tm], axis=0)

        matrix_tm = sklearn.metrics.confusion_matrix(
            y_true_tm,
            y_pred_tm,
            normalize=normalize
        )

        matrix = {'secstr': matrix_secstr, 'tm': matrix_tm}
        return matrix

    def predict_from_sequence(self, sequence, full_return=True):
        """Predict the secondary structure and transmembrane information
        from a given string

        Args:
            sequence: a sequence of amino acids
            full_return: boolean that changes the type of return

        Returns:
            ret: if full_return is True, returns a dictionary containing
                the raw outputs from the neural networks
                if full_return is False, return a dictionary containing
                two strings representing the most probable secondary structure
                and transmembrane parts from the models prediction.

        """

        sequence = np.array(encode_x(sequence, self.vocab_x, self.window))
        pred_secstr, pred_tm = self.predict(sequence)

        if full_return:
            ret = {'secstr': pred_secstr, 'pred_tm': pred_tm}

        else:
            pred_secstr = pred_secstr.argmax(axis=1)
            pred_secstr = [self.inv_vocabs[0][p] for p in pred_secstr]
            pred_secstr = "".join(pred_secstr)
            pred_secstr = "X"*(self.window//2) + \
                          pred_secstr + \
                          "X"*(self.window//2)

            pred_tm = pred_tm >= .5
            pred_tm = [self.inv_vocabs[1][int(p)] for p in pred_tm]
            pred_tm = "".join(pred_tm)
            pred_tm = "X"*(self.window//2) + pred_tm + "X"*(self.window//2)

            ret = {'secstr': pred_secstr, 'tm': pred_tm}

        return ret

    def predict_from_sequence_batch(self, sequences, full_return=True):
        return [self.predict_from_sequence(seq, full_return=full_return)
                for seq in sequences]
