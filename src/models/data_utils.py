import os 
import numpy as np
import pandas as pd 


def vocab(seq):
    """Automatically a dictionary to encode sequences from a list of sequences

    Args:
        seq: list of strings 

    Returns:
        dictionary containing (characters, integer) pairs

    """

    ret = {}
    i = 0
    for s in seq:
        for char in s:
            if char not in ret:
                ret[char] = i
                i += 1
    return ret


def vocab_prot():
    """Returns a dictionary to encode an amino acid sequence into a list of 
    integers
    """

    alphabet = 'ARNDCQEGHILKMFPSTWYVUO'
    voc = {char: i for i, char in enumerate(alphabet)}
    return voc


def vocab_tm():
    """Returns a dictionary to encode transmembrane information into a list 
    of integers
    """

    return {'s': 0, 'M': 1}


def vocab_secstr():
    """Returns a dictionary to encode secondary structure into a list of 
    integers
    """

    return {' ': 0, 'b': 0, 's': 1, 'h': 2}


def encode_x(seq, vocab, window):
    """Encode an amino acid sequence with a given vocab and a given window

    Notes:
        window should be odd
        same window should be used for encode_x and encode_y
    
    Args:
        seq: string to encode
        vocab: dictionary of (character: integers) pairs used to encode the
            string
        window: window to use to split the string into substrings

    Returns:
        A 2D array of integers, first dimension being the length of the
            window. Each element is the encoding of a substring of fixed window
            length into a list of integers.

    """

    seq = [seq[i-window//2:i+window//2+1]
           for i in range(window//2, len(seq) - window//2)]
    seq = [[vocab[char] for char in s] for s in seq]
    return seq


def encode_y(seq, vocab, window):
    """Encode a secondary structure/transmembrane sequence with a given vocab 
    and a given window

    Notes:
        window should be odd
        same window should be used for encode_x and encode_y

    Args:
        seq: string to encode
        vocab: dictionary of (character: integers) pairs used to encode the
            string
        window: window to use to split the string into substrings

    Returns:
        A 2D array of integers, first dimension being the length of the
            window. Each element is the encoding of a substring of fixed window
            length into a list of integers.

    """
    seq = [vocab[char] for char in seq]
    seq = seq[window//2:-window//2+1]
    return seq


def transform_df(df, encode_x, encode_y, vocab_x, vocab_y, window):
    """encode the Sequences and Targets columns from a dataframe into 
    data manageable by the neural network
    """
    x = []
    y = []
    seq = list(df['Sequences'])
    for s in seq:
        x += encode_x(s, vocab_x, window)
    target = list(df['Targets'])
    for t in target:
        y += encode_y(t, vocab_y, window)
    x = np.array(x)
    y = np.array(y)
    p = np.random.permutation(len(x))
    return x[p], y[p]


def check_dir(directory):
    """Check if directory exists, is a directory and creates it if necessary

    Args:
        directory: string, path to directory

    """

    if not os.path.exists(directory):
        os.mkdir(directory)
    elif not os.path.isdir(directory):
        os.remove(directory)
        os.mkdir(directory)
