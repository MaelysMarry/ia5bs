from models.model import ProtModel
from tensorflow import keras
from tensorflow.keras import layers


class LSTMmodel(ProtModel):

    def __init__(self, window, embedding_size=20, alphabet_size=21):
        common_layers = [
            layers.Input(shape=[window]),
            layers.Embedding(alphabet_size, embedding_size),
            layers.Bidirectional(layers.LSTM(64, return_sequences=True))
        ]
        secstr_layers = [
            layers.Bidirectional(layers.LSTM(32, return_sequences=False)),
            layers.Dense(3, activation='sigmoid')
        ]
        tm_layers = [
            layers.Bidirectional(layers.LSTM(32, return_sequences=False)),
            layers.Dense(1, activation='sigmoid')
        ]
        super().__init__(common_layers, [secstr_layers, tm_layers], window)
