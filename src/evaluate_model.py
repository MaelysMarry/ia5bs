import os
import pickle as pkl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from argparse import ArgumentParser
from models.model import ProtModel


def main():

    parser = ArgumentParser(
        description="evaluate a given model"
    )

    parser.add_argument('-m',
                        help="model_directory",
                        required=True)
    parser.add_argument('-t',
                        help='testing dataset directory',
                        required=False)
    parser.add_argument('-c',
                        help='number of secondary structures to keep',
                        type=int,
                        required=False,
                        default=None)

    args = parser.parse_args()
    model_dir = args.m
    test_dir = args.t
    cut = args.c

    model = ProtModel.load(model_dir)

    for m in model:
        print(m.summary())

    results = pkl.load(open(os.path.join(model_dir, 'results.pkl'), 'rb'))

    # plot loss
    f, axs = plt.subplots(1, 2)
    axs[0].plot(results['secstr']['train_loss'], label='training loss')
    axs[0].plot(results['secstr']['val_loss'], label='validation loss')
    axs[1].plot(results['tm']['train_loss'], label='training loss')
    axs[1].plot(results['tm']['val_loss'], label='validation loss')
    for ax in axs:
        ax.set_xlabel('Epochs')
        ax.set_ylabel('loss')
        ax.legend()
    axs[0].set_title('Secondary structure loss')
    axs[1].set_title('Transmembrane loss')
    f.suptitle('Value of loss function during training')
    print("Saving Losses.png")
    plt.savefig(os.path.join(model_dir, "Losses.png"))

    titles = {'secstr': 'Secondary Structure', 'tm': 'Transmembrane'}

    # plot other metrics
    for i, model_name in enumerate(results):
        for metric in results[model_name]['metrics']:
            f, ax = plt.subplots()
            ax.plot(results[model_name]['metrics'][metric])
            ax.set_xlabel('Epochs')
            ax.set_ylabel(metric)
            ax.set_title(f"{metric} of {titles[model_name]} model")
            print(f"Saving {model_name}_{metric}.png")
            plt.savefig(os.path.join(model_dir, f"{model_name}_{metric}.png"))

    df_secstr = (pd.read_csv(os.path.join(test_dir, 'secstr_test.csv'))
                   .rename(columns={'str2aire': 'Targets'}))
    df_tm = (pd.read_csv(os.path.join(test_dir, 'tm_test.csv'))
               .rename(columns={'Membrane': 'Targets'}))

    if cut:
        df_secstr = df_secstr.sample(n=cut)

    model.load_data(df_secstr, df_tm)
    matrices = model.confusion_matrix(normalize='true')

    indices = {'secstr': ['coudes/sans structure', 'feuillets', 'hélices'],
               'tm': ['en solution', 'transmembranaire']}

    for model_name in matrices:
        f, ax = plt.subplots()
        df_cm = pd.DataFrame(matrices[model_name],
                             index=indices[model_name],
                             columns=indices[model_name])
        sns.heatmap(df_cm, annot=True, ax=ax)
        ax.set_title(f"Confusion matrix of {titles[model_name]} model")
        ax.set_xlabel('Predicted labels')
        ax.set_ylabel('True labels')
        plt.savefig(os.path.join(model_dir, f"confusion_{model_name}.png"))


if __name__ == '__main__':
    main()
