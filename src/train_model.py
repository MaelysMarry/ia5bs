from models.LSTM import LSTMmodel
from models.CNN import CNNmodel
import pandas as pd
import tensorflow.keras as keras
import os
from argparse import ArgumentParser
import pickle as pkl


def main():

    parser = ArgumentParser(
        description="This script trains a model predicting secondary structure"
                    "and transmembrane data"
    )

    parser.add_argument("-d",
                        help="data directory",
                        required=True)
    parser.add_argument("-t",
                        help="number of epochs",
                        type=int,
                        required=True)
    parser.add_argument("-o",
                        help="directory for saving the model",
                        required=True)
    parser.add_argument("-c",
                        help="number of secondary structures to keep",
                        required=False,
                        type=int,
                        default=None)
    parser.add_argument("-b1",
                        help="batch_size for secondary structures",
                        required=False,
                        type=int,
                        default=100)
    parser.add_argument("-b2",
                        help="batch_size for transmembrane structures",
                        required=False,
                        type=int,
                        default=100)
    parser.add_argument("-e",
                        help="applies early stopping to the training",
                        action="store_true")

    args = parser.parse_args()
    data_dir = args.d
    epochs = args.t
    save_dir = args.o
    keep = args.c
    b1 = args.b1
    b2 = args.b2
    early_stopping = args.e

    model = CNNmodel(window=21)

    df_secstr = (pd.read_csv(os.path.join(data_dir, 'secstr_train.csv'))
                   .rename(columns={'str2aire': 'Targets'}))
    df_tm = (pd.read_csv(os.path.join(data_dir, 'tm_train.csv'))
               .rename(columns={'Membrane': 'Targets'}))

    if keep:
        df_secstr = df_secstr.sample(n=keep)

    model.compile(
        losses=[
            keras.losses.SparseCategoricalCrossentropy(from_logits=False),
            keras.losses.BinaryCrossentropy(from_logits=False)
        ],
        opts=[
            keras.optimizers.Adam(learning_rate=1e-5),
            keras.optimizers.Adam(learning_rate=1e-5)
        ],
        metrics=[
            [keras.metrics.SparseCategoricalAccuracy(name='Accuracy')],
            [keras.metrics.Recall(name='Recall'),
             keras.metrics.AUC(name="AUC"),
             keras.metrics.Precision(name='Precision')]
        ]
    )

    for m in model:
        print(m.summary())

    model.load_data(df_secstr, df_tm, split=0.8/0.9)  # Quick and dirty fix
    secstr, tm = model.fit_from_data(epochs=epochs, batch_sizes=(b1, b2),
                                     early_stopping=early_stopping)

    model_number = len(os.listdir(save_dir))
    save_dir = os.path.join(save_dir, f"model_{model_number:03d}")
    os.mkdir(save_dir)
    model.save(save_dir)
    pkl.dump({'secstr': secstr, 'tm': tm},
             open(os.path.join(save_dir, "results.pkl"), 'wb'))


if __name__ == '__main__':
    main()
