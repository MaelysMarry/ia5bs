import os
from argparse import ArgumentParser
from models.model import ProtModel
from Bio import SeqIO


def main():

    parser = ArgumentParser(
        description="predict sequences from a fasta file"
    )

    parser.add_argument('-m',
                        help="model_directory",
                        required=True)
    parser.add_argument('-f',
                        help="fasta file",
                        required=True)

    args = parser.parse_args()
    model_dir = args.m
    fasta = args.f

    model = ProtModel.load(model_dir)

    for m in model:
        print(m.summary())

    ids = []
    seqs = []

    for record in SeqIO.parse(fasta, "fasta"):
        ids.append(record.id)
        seqs.append(record.seq)

    preds = model.predict_from_sequence_batch(seqs, full_return=False)

    secstr = []
    tm = []

    for pred in preds:
        secstr.append(pred['secstr'])
        tm.append(pred['tm'])

    with open(f"{fasta.split('.')[-2]}.secstr", 'w') as f:
        for sec, i in zip(secstr, ids):
            f.write(f">{i}\n")
            f.write(f"{sec}\n")

    with open(f"{fasta.split('.')[-2]}.tm", 'w') as f:
        for t, i in zip(tm, ids):
            f.write(f">{i}\n")
            f.write(f"{t}\n")


if __name__ == '__main__':
    main()
