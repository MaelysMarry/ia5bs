mkdir saved_models
python3 src/train_model.py -d data/train/ -o saved_models/ -t 500 -b1 50000 -b2 500 -e
python3 src/evaluate_model.py -m saved_models/model_000 -t data/test
